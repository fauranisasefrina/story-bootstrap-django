from django.urls import path
from . import views

app_name = 'homepage'

urlpatterns = [
    path('', views.efrin, name='efrin'),
    path('organisasi-kepanitiaan/', views.orkep, name='orkep'),
    # dilanjutkan ...
]